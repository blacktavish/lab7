import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;

public class Main{
    private final static String FILE_NAME = "resume.pdf";
    public static int COLUMNS = 2;
    private static final String TITLE = "RESUME";
    private static final String FIRST_NAME = "Jakub";
    private static final String LAST_NAME = "Los";
    private static final String EDUCATION = "2014-2018: ZSME in Tarnow \n" + "2018-now: PWSZ";
    private static final String PROFFESION = "IT clerk";
    private static final String SUMMARY = "I am young IT student.";

    public static void main(String[] args) {

        Document document = new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream(new File(FILE_NAME)));
            document.open();
            document.add(Image.getInstance("los.jpg"));
            document.add(createTable());
            document.close();

            System.out.println("Done");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static PdfPCell createCell(String text){
        PdfPCell cell = new PdfPCell();
        cell.addElement(new Paragraph(text));
        cell.setPadding(5);

        return  cell;
    }

    public static PdfPCell createHeader(String text){
        PdfPCell cell = new PdfPCell();
        Font font = new Font(Font.FontFamily.TIMES_ROMAN,20,Font.BOLD);
        Paragraph p = new Paragraph(text,font);

        cell.addElement(p);
        cell.setMinimumHeight(150);
        p.setAlignment(Element.ALIGN_CENTER);
        cell.setColspan(2);
        cell.setBorder(Rectangle.NO_BORDER);

        return cell;
    }
    public static PdfPTable createTable(){
        PdfPTable table = new PdfPTable(COLUMNS);

        table.addCell(createHeader(TITLE));
        table.addCell(createCell("First Name: "));
        table.addCell(createCell(FIRST_NAME));
        table.addCell(createCell("Last Name: "));
        table.addCell(createCell(LAST_NAME));
        table.addCell(createCell("Education: "));
        table.addCell(createCell(EDUCATION));
        table.addCell(createCell("Proffesion: "));
        table.addCell(createCell(PROFFESION));
        table.addCell(createCell("Summary: "));
        table.addCell(createCell(SUMMARY));

        return table;
    }

}